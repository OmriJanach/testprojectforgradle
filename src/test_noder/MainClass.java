package test_noder;

public class MainClass {
	
	private int a, b;
	
	
	public MainClass(int a, int b) {
		this.a = a;
		this.b = b;
	}
	
	public void setA(int num) {
		this.a = num;
	}
	
	public void setB(int num) {
		this.b = num;
	}
	
	public int getA() {
		return a;
	}
	
	public int getB() {
		return b;
	}
	
	public int result() {
		return this.a + this.b;
	}

}
